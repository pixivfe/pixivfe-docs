# pixivfe-docs

Repository for [PixivFE](https://codeberg.org/VnPower/PixivFE) documentation. Deployed to [pixivfe.pages.dev](https://pixivfe.pages.dev/) using [Cloudflare Pages](https://developers.cloudflare.com/pages/).

## Development

Clone the `pixivfe-docs` repository and navigate to the directory:

```bash
git clone https://gitlab.com/pixivfe/pixivfe-docs.git && cd pixivfe-docs
```

### Local `mkdocs-material` installation

```bash
# You might want to create a virtualenv first
pip install -r requirements.txt
mkdocs serve
```

### With docker

```bash
docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material:latest
```

## License

Licensed under [GNU Free Documentation License v1.3](https://www.gnu.org/licenses/fdl-1.3.html) or later.
