# PixivFE

A privacy-respecting alternative front-end for Pixiv that doesn't suck.

Ready-to-use running instances are available at the [instance list](instance-list.md).

[![Get it on Codeberg](https://get-it-on.codeberg.org/get-it-on-blue-on-white.png){ width="200" }](https://codeberg.org/VnPower/PixivFE)

[![Go Report Card](https://goreportcard.com/badge/codeberg.org/vnpower/pixivfe/v2)](https://goreportcard.com/report/codeberg.org/vnpower/pixivfe)

## Features

- **Lightweight** - both the interface and the code
- **Privacy-first** - the server will do the work for you
- **No bloat** - we only serve HTML, CSS and minimal JS code
- **Open source** - you can trust me!

## License

PixivFE is licensed under [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0.txt).
