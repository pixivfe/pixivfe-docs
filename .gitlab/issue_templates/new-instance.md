## Name

(The name of the instance.)

## URL

(The full URL of the instance. For example, pixivfe.example.com)

## Country

(The country where the instance is hosted.)

## Cloudflare proxy

(Specify 'Yes' if the instance is proxied behind Cloudflare, otherwise 'No'.)

## Analytics

(Specify 'Yes' if the instance runs analytics, otherwise 'No'.)

/label ~"Add instance"
